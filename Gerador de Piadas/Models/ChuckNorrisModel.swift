//
//  ChuckNorrisModel.swift
//  Gerador de Piadas
//
//  Created by Lucas Gomes on 15/02/23.
//

import Foundation

struct ChuckNorrisModel: Decodable {
    let icon_url: String?
    let id: String?
    let url: String?
    let value: String?
}
