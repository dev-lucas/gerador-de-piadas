//
//  ButtonTitlesEnum.swift
//  Gerador de Piadas
//
//  Created by Lucas Gomes on 18/02/23.
//

import Foundation

enum ButtonTitleEnum: String {
    case generateNewJokeDefault = "Gerar nova piada"
}
