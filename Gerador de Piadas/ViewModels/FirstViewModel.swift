//
//  FirstViewModel.swift
//  Gerador de Piadas
//
//  Created by Lucas Gomes on 18/02/23.
//

import UIKit

final class FirstViewModel {
    
    private lazy var chuckNorrisService = ChuckNorrisService()
    
    func getJoke(_ jokeLabel: UILabel?, _ jokeButton: UIButton?) {
        guard let label = jokeLabel, let button = jokeButton else { return }

        ButtonUtil.handleFeedbacks(showLoading: true, button: button)
        
        chuckNorrisService.getJoke { joke, error in
            ButtonUtil.handleFeedbacks(showLoading: false, button: button, title: ButtonTitleEnum.generateNewJokeDefault.rawValue)
            error == nil ? (label.text = joke?.value) : (label.text = error?.localizedDescription)
        }
    }
}
