//
//  FirstViewController.swift
//  Gerador de Piadas
//
//  Created by Lucas Gomes on 15/02/23.
//

import UIKit

final class FirstViewController: UIViewController {

    @IBOutlet weak var jokeLabel: UILabel?
    private lazy var firstViewModel = FirstViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func generateNewJokeButton(_ sender: UIButton) {
        firstViewModel.getJoke(jokeLabel, sender)
    }
}
