//
//  ChuckNorrisService.swift
//  Gerador de Piadas
//
//  Created by Lucas Gomes on 17/02/23.
//

import Alamofire
import Foundation

final class ChuckNorrisService {
    
    private let url: String = "https://matchilling-chuck-norris-jokes-v1.p.rapidapi.com/jokes/random"
    private let headers: HTTPHeaders = ["Accept": "application/json", "X-RapidAPI-Key": "4a89549452msh31d3b8180f27191p1aa3a9jsn81d47092cab6", "X-RapidAPI-Host": "matchilling-chuck-norris-jokes-v1.p.rapidapi.com"]
    
    func getJoke(completion: @escaping (_ joke: ChuckNorrisModel?, _ error: Error?) -> Void) {
        AF.request(url, method: .get, headers: headers).responseDecodable(of: ChuckNorrisModel.self) { response in
            switch response.result {
            case .success(let result):
                completion(result, nil)
                break
            case .failure(let error):
                completion(nil, error)
                break
            }
        }
    }
}
