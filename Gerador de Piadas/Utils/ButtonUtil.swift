//
//  ButtonUtil.swift
//  Gerador de Piadas
//
//  Created by Lucas Gomes on 17/02/23.
//

import UIKit

struct ButtonUtil {
    static func handleFeedbacks(showLoading: Bool, button: UIButton, title: String? = nil) {
           button.configuration?.showsActivityIndicator = showLoading
           button.configuration?.title = title
           button.isEnabled = !showLoading
       }
}
