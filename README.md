<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<a name="readme-top"></a>
<br />

<!-- ## Teste -->

<div align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/43522958/git_project_logos__1_.png?width=650" alt="Logo" width="200" height="200">
  </a>

### Gerador de Piadas

  <p align="center">
    Gerador de piadas integrado com Chuck Norris API
    <br />
    <a href="https://gitlab.com/users/dev-lucas/projects"><strong>Ver mais projetos »</strong></a>
    <br />
    <br />
  </p>
</div>

<details>
  <summary>Sumário</summary>
  <ol>
    <li>
      <a href="#sobre-o-projeto">Sobre o projeto</a>
    </li>
    <li>
      <a href="#como-executar-o-projeto">Como executar o projeto</a>
      <ul>
        <li><a href="#pré-requisitos">Pré-requisitos</a></li>
        <li><a href="#instalação">Instalação</a></li>
      </ul>
    </li>
    <li><a href="#contato">Contato</a></li>
  </ol>
</details>

<br>

---

<br>

### Sobre o Projeto

O projeto consiste em uma tela. Quando o usuário clicar em "Gerar nova piada", uma requisição é enviada para a api e o retorno é apresentado em uma label acima do botão.


![](https://i.postimg.cc/rwV6RMBm/Simulator-Screen-Shot-i-Phone-14-Pro-2023-02-18-at-15-59-34.png)

O que voce pode encontrar nos códigos:
* Exemplo de MVVM.
* Configuração básica para definir a view principal.
* Uso básico de constraint.
* Uso básico de enum.
* Integração utilizando Alamofire.


<br>

<div align="right">(<a href="#gerador-de-piadas">voltar para o início</a>)</div>

---

<br>


### Como executar o projeto

Este é um exemplo de como você pode executar o projeto localmente.
Para colocar uma cópia local em funcionamento, siga as etapas abaixo.

#### Pré-requisitos

Este é um exemplo dos pré-requisitos que você precisa ter

- Xcode 11.1
- Este aplicativo está usando Swift 5

#### Instalação

Este é um exemplo de como instalar o projeto

1. Clonar o repositório
   ```
   git clone https://gitlab.com/dev-lucas/gerador-de-piadas
   ```

<br>

<div align="right">(<a href="#gerador-de-piadas">voltar para o início</a>)</div>

---

<br>


### Contato

Lucas Gomes Vieira - lucasgomes114@gmail.com

Link do projeto: [https://gitlab.com/dev-lucas/gerador-de-piadas](https://gitlab.com/dev-lucas/gerador-de-piadas)

<div align="right">(<a href="#gerador-de-piadas">voltar para o início</a>)</div>

---

<br>
